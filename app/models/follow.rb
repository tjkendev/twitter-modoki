class Follow < ApplicationRecord
  belongs_to :user
  belongs_to :follow_user, foreign_key: 'follow_user_id', class_name: 'User'

  class << self
    def find_relation(from_id, to_id)
      follow = Follow.find_by(user_id: from_id, follow_user_id: to_id)
      return follow.present?
    end

    def user_ids
      all.map{|follow| follow[:user_id]}
    end

    def follow_ids
      all.map{|follow| follow[:follow_user_id]}
    end
  end
end
