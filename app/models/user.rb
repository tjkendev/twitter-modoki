class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :tweets

  has_many :follows
  has_many :user, through: :follows


  validates :user_name, presence: true

  def name
    user_name.empty? ? '<empty>' : user_name
  end

end
