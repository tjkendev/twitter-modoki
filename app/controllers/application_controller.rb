class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private
  def auth_check
    redirect_to root_path, :alert => 'アクセス権限がありません' unless user_signed_in?
  end
end
