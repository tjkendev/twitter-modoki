class TweetController < ApplicationController

  before_filter :authenticate_user!

  # ## ツイート作成画面
  def new
    Rails.logger.debug "current_user #{current_user}"
    @tweet = Tweet.new(user_id: current_user.id) unless @tweet.present?
  end

  # ## ツイートの投稿
  def create
    @tweet = Tweet.create(input_params)
    @tweet[:user_id] = current_user.id
    if @tweet.save!
      redirect_to timeline_index_path, :notice => 'ツイートに成功しました'
    else
      redirect_to tweet_new_path(@tweet), :alert => 'ツイートに失敗しました'
    end
  end

  # ## ツイートの削除
  def destroy
    @tweet = Tweet.find_by(id: params[:tweet_id])
    puts @tweet
    if @tweet.blank? || @tweet[:user_id] != current_user.id
      redirect_to :back, :alert => 'ツイートを削除することができません'
    else
      if @tweet.destroy!
        redirect_to :back, :notice => 'ツイート削除に成功しました'
      else
        redirect_to :back, :alert => 'ツイート削除に失敗しました'
      end
    end
  end

  private

  def input_params
    # Tweetにはmessageが渡される
    params.require(:tweet).permit(:message)
  end
end
