class UserController < ApplicationController
  def index
    @users = User.all
    if current_user.present?
      @follow_ids = current_user.follows.follow_ids
      Rails.logger.debug @follow_ids
    else
      @follow_ids = nil
    end
  end

  def show
    Rails.logger.debug params
    @user = User.find_by(id: params[:user_id])
    redirect_to user_index_path, :alert => '無効なユーザーです' unless @user.present?

    @tweets = Tweet.where(user_id: params[:user_id])
  end
end
