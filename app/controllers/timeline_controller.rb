class TimelineController < ApplicationController
  def index
    if !current_user.present?
      @tweets = Tweet.all
    else
      follow_ids = current_user.follows.follow_ids
      follow_ids << current_user.id
      @tweets = Tweet.where(user_id: follow_ids)
    end
  end

  def show
  end
end
