class FollowController < ApplicationController

  before_filter :authenticate_user!

  # ## フォロー一覧
  def index
    @follows = current_user.follows
    @follow_ids = @follows.follow_ids
    @followers = Follow.where(follow_user_id: current_user.id)
  end

  # ## フォロー処理
  def create
    @follow_user = User.find_by(id: params[:follow_user_id])
    if @follow_user.blank?
      redirect_to root_path, :alert => '無効なユーザーです'
      return
    end
    if Follow.find_relation(current_user.id, params[:follow_user_id])
      redirect_to root_path, :alert => 'フォロー済みです'
      return
    end

    follow = Follow.create(user_id: current_user.id, follow_user_id: params[:follow_user_id])
    if follow.save!
      @message = "#{@follow_user.name}をフォローしました"
    else
      @message = 'フォローに失敗しました'
    end
  end

  # ## アンフォロー処理
  def destroy
    @follow_user = User.find_by(id: params[:follow_user_id])
    if @follow_user.blank?
      redirect_to root_path, :alert => '無効なユーザーです'
      return
    end
    unless Follow.find_relation(current_user.id, params[:follow_user_id])
      redirect_to root_path, :alert => 'フォローしていません'
      return
    end

    follow = Follow.find_by(user_id: current_user.id, follow_user_id: params[:follow_user_id])
    if follow.destroy!
      @message = "#{@follow_user.name}をアンフォローしました"
    else
      @message = 'アンフォローに失敗しました'
    end
  end

  def redirect
    redirect_to root_path
  end
end
