json.extract! memo, :id, :user_id, :header, :body, :created_at, :updated_at
json.url memo_url(memo, format: :json)
