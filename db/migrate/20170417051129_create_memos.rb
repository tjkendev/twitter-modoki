class CreateMemos < ActiveRecord::Migration[5.0]
  def change
    create_table :memos do |t|
      t.integer :user_id
      t.string :header
      t.text :body

      t.timestamps
    end
  end
end
