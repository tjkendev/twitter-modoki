Rails.application.routes.draw do


  resources :memos
  devise_for :users, :controllers => {
      :registrations => 'users/registrations',
      :sessions => 'users/sessions'
  }
  root 'timeline#index'

  # ## タイムライン
  get 'timeline/index'

  # ## ユーザー一覧、詳細
  get 'user/index'
  match 'user/:user_id', :controller => :user, :action => :show, :via => :get

  # ## ツイート投稿, 詳細
  get 'tweet/new'
  match 'tweet/new', :controller => :tweet, :action => :create, :via => :post
  match 'tweet/:tweet_id', :controller => :tweet, :action => :show, :via => :get
  match 'tweet/delete/:tweet_id', :controller => :tweet, :action => :destroy, :via => :delete

  # ## フォロー処理
  get 'follow/index'
  match 'follow', :controller => :follow, :action => :create, :via => :post
  match 'follow', :controller => :follow, :action => :redirect, :via => :get
  match 'follow', :controller => :follow, :action => :destroy, :via => :delete

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
